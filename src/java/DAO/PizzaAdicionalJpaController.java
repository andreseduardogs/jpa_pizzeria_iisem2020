/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DAO.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DTO.Pizza;
import DTO.IngredienteAdicional;
import DTO.PizzaAdicional;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author madar
 */
public class PizzaAdicionalJpaController implements Serializable {

    public PizzaAdicionalJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(PizzaAdicional pizzaAdicional) throws IllegalOrphanException, PreexistingEntityException, Exception {
        List<String> illegalOrphanMessages = null;
        Pizza pizzaOrphanCheck = pizzaAdicional.getPizza();
        if (pizzaOrphanCheck != null) {
            PizzaAdicional oldPizzaAdicionalOfPizza = pizzaOrphanCheck.getPizzaAdicional();
            if (oldPizzaAdicionalOfPizza != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("The Pizza " + pizzaOrphanCheck + " already has an item of type PizzaAdicional whose pizza column cannot be null. Please make another selection for the pizza field.");
            }
        }
        if (illegalOrphanMessages != null) {
            throw new IllegalOrphanException(illegalOrphanMessages);
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pizza pizza = pizzaAdicional.getPizza();
            if (pizza != null) {
                pizza = em.getReference(pizza.getClass(), pizza.getIdPizza());
                pizzaAdicional.setPizza(pizza);
            }
            IngredienteAdicional idIngrediente = pizzaAdicional.getIdIngrediente();
            if (idIngrediente != null) {
                idIngrediente = em.getReference(idIngrediente.getClass(), idIngrediente.getIdIngrediente());
                pizzaAdicional.setIdIngrediente(idIngrediente);
            }
            em.persist(pizzaAdicional);
            if (pizza != null) {
                pizza.setPizzaAdicional(pizzaAdicional);
                pizza = em.merge(pizza);
            }
            if (idIngrediente != null) {
                idIngrediente.getPizzaAdicionalList().add(pizzaAdicional);
                idIngrediente = em.merge(idIngrediente);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPizzaAdicional(pizzaAdicional.getIdPizza()) != null) {
                throw new PreexistingEntityException("PizzaAdicional " + pizzaAdicional + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(PizzaAdicional pizzaAdicional) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PizzaAdicional persistentPizzaAdicional = em.find(PizzaAdicional.class, pizzaAdicional.getIdPizza());
            Pizza pizzaOld = persistentPizzaAdicional.getPizza();
            Pizza pizzaNew = pizzaAdicional.getPizza();
            IngredienteAdicional idIngredienteOld = persistentPizzaAdicional.getIdIngrediente();
            IngredienteAdicional idIngredienteNew = pizzaAdicional.getIdIngrediente();
            List<String> illegalOrphanMessages = null;
            if (pizzaNew != null && !pizzaNew.equals(pizzaOld)) {
                PizzaAdicional oldPizzaAdicionalOfPizza = pizzaNew.getPizzaAdicional();
                if (oldPizzaAdicionalOfPizza != null) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("The Pizza " + pizzaNew + " already has an item of type PizzaAdicional whose pizza column cannot be null. Please make another selection for the pizza field.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (pizzaNew != null) {
                pizzaNew = em.getReference(pizzaNew.getClass(), pizzaNew.getIdPizza());
                pizzaAdicional.setPizza(pizzaNew);
            }
            if (idIngredienteNew != null) {
                idIngredienteNew = em.getReference(idIngredienteNew.getClass(), idIngredienteNew.getIdIngrediente());
                pizzaAdicional.setIdIngrediente(idIngredienteNew);
            }
            pizzaAdicional = em.merge(pizzaAdicional);
            if (pizzaOld != null && !pizzaOld.equals(pizzaNew)) {
                pizzaOld.setPizzaAdicional(null);
                pizzaOld = em.merge(pizzaOld);
            }
            if (pizzaNew != null && !pizzaNew.equals(pizzaOld)) {
                pizzaNew.setPizzaAdicional(pizzaAdicional);
                pizzaNew = em.merge(pizzaNew);
            }
            if (idIngredienteOld != null && !idIngredienteOld.equals(idIngredienteNew)) {
                idIngredienteOld.getPizzaAdicionalList().remove(pizzaAdicional);
                idIngredienteOld = em.merge(idIngredienteOld);
            }
            if (idIngredienteNew != null && !idIngredienteNew.equals(idIngredienteOld)) {
                idIngredienteNew.getPizzaAdicionalList().add(pizzaAdicional);
                idIngredienteNew = em.merge(idIngredienteNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pizzaAdicional.getIdPizza();
                if (findPizzaAdicional(id) == null) {
                    throw new NonexistentEntityException("The pizzaAdicional with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            PizzaAdicional pizzaAdicional;
            try {
                pizzaAdicional = em.getReference(PizzaAdicional.class, id);
                pizzaAdicional.getIdPizza();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pizzaAdicional with id " + id + " no longer exists.", enfe);
            }
            Pizza pizza = pizzaAdicional.getPizza();
            if (pizza != null) {
                pizza.setPizzaAdicional(null);
                pizza = em.merge(pizza);
            }
            IngredienteAdicional idIngrediente = pizzaAdicional.getIdIngrediente();
            if (idIngrediente != null) {
                idIngrediente.getPizzaAdicionalList().remove(pizzaAdicional);
                idIngrediente = em.merge(idIngrediente);
            }
            em.remove(pizzaAdicional);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<PizzaAdicional> findPizzaAdicionalEntities() {
        return findPizzaAdicionalEntities(true, -1, -1);
    }

    public List<PizzaAdicional> findPizzaAdicionalEntities(int maxResults, int firstResult) {
        return findPizzaAdicionalEntities(false, maxResults, firstResult);
    }

    private List<PizzaAdicional> findPizzaAdicionalEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(PizzaAdicional.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public PizzaAdicional findPizzaAdicional(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(PizzaAdicional.class, id);
        } finally {
            em.close();
        }
    }

    public int getPizzaAdicionalCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<PizzaAdicional> rt = cq.from(PizzaAdicional.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAO;

import DAO.exceptions.IllegalOrphanException;
import DAO.exceptions.NonexistentEntityException;
import DTO.Pizza;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import DTO.Sabor;
import DTO.Tipo;
import DTO.PizzaAdicional;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author madar
 */
public class PizzaJpaController implements Serializable {

    public PizzaJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Pizza pizza) {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Sabor idSabor = pizza.getIdSabor();
            if (idSabor != null) {
                idSabor = em.getReference(idSabor.getClass(), idSabor.getIdSabor());
                pizza.setIdSabor(idSabor);
            }
            Tipo idTipo = pizza.getIdTipo();
            if (idTipo != null) {
                idTipo = em.getReference(idTipo.getClass(), idTipo.getId());
                pizza.setIdTipo(idTipo);
            }
            PizzaAdicional pizzaAdicional = pizza.getPizzaAdicional();
            if (pizzaAdicional != null) {
                pizzaAdicional = em.getReference(pizzaAdicional.getClass(), pizzaAdicional.getIdPizza());
                pizza.setPizzaAdicional(pizzaAdicional);
            }
            em.persist(pizza);
            if (idSabor != null) {
                idSabor.getPizzaList().add(pizza);
                idSabor = em.merge(idSabor);
            }
            if (idTipo != null) {
                idTipo.getPizzaList().add(pizza);
                idTipo = em.merge(idTipo);
            }
            if (pizzaAdicional != null) {
                Pizza oldPizzaOfPizzaAdicional = pizzaAdicional.getPizza();
                if (oldPizzaOfPizzaAdicional != null) {
                    oldPizzaOfPizzaAdicional.setPizzaAdicional(null);
                    oldPizzaOfPizzaAdicional = em.merge(oldPizzaOfPizzaAdicional);
                }
                pizzaAdicional.setPizza(pizza);
                pizzaAdicional = em.merge(pizzaAdicional);
            }
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Pizza pizza) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pizza persistentPizza = em.find(Pizza.class, pizza.getIdPizza());
            Sabor idSaborOld = persistentPizza.getIdSabor();
            Sabor idSaborNew = pizza.getIdSabor();
            Tipo idTipoOld = persistentPizza.getIdTipo();
            Tipo idTipoNew = pizza.getIdTipo();
            PizzaAdicional pizzaAdicionalOld = persistentPizza.getPizzaAdicional();
            PizzaAdicional pizzaAdicionalNew = pizza.getPizzaAdicional();
            List<String> illegalOrphanMessages = null;
            if (pizzaAdicionalOld != null && !pizzaAdicionalOld.equals(pizzaAdicionalNew)) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("You must retain PizzaAdicional " + pizzaAdicionalOld + " since its pizza field is not nullable.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            if (idSaborNew != null) {
                idSaborNew = em.getReference(idSaborNew.getClass(), idSaborNew.getIdSabor());
                pizza.setIdSabor(idSaborNew);
            }
            if (idTipoNew != null) {
                idTipoNew = em.getReference(idTipoNew.getClass(), idTipoNew.getId());
                pizza.setIdTipo(idTipoNew);
            }
            if (pizzaAdicionalNew != null) {
                pizzaAdicionalNew = em.getReference(pizzaAdicionalNew.getClass(), pizzaAdicionalNew.getIdPizza());
                pizza.setPizzaAdicional(pizzaAdicionalNew);
            }
            pizza = em.merge(pizza);
            if (idSaborOld != null && !idSaborOld.equals(idSaborNew)) {
                idSaborOld.getPizzaList().remove(pizza);
                idSaborOld = em.merge(idSaborOld);
            }
            if (idSaborNew != null && !idSaborNew.equals(idSaborOld)) {
                idSaborNew.getPizzaList().add(pizza);
                idSaborNew = em.merge(idSaborNew);
            }
            if (idTipoOld != null && !idTipoOld.equals(idTipoNew)) {
                idTipoOld.getPizzaList().remove(pizza);
                idTipoOld = em.merge(idTipoOld);
            }
            if (idTipoNew != null && !idTipoNew.equals(idTipoOld)) {
                idTipoNew.getPizzaList().add(pizza);
                idTipoNew = em.merge(idTipoNew);
            }
            if (pizzaAdicionalNew != null && !pizzaAdicionalNew.equals(pizzaAdicionalOld)) {
                Pizza oldPizzaOfPizzaAdicional = pizzaAdicionalNew.getPizza();
                if (oldPizzaOfPizzaAdicional != null) {
                    oldPizzaOfPizzaAdicional.setPizzaAdicional(null);
                    oldPizzaOfPizzaAdicional = em.merge(oldPizzaOfPizzaAdicional);
                }
                pizzaAdicionalNew.setPizza(pizza);
                pizzaAdicionalNew = em.merge(pizzaAdicionalNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = pizza.getIdPizza();
                if (findPizza(id) == null) {
                    throw new NonexistentEntityException("The pizza with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Pizza pizza;
            try {
                pizza = em.getReference(Pizza.class, id);
                pizza.getIdPizza();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The pizza with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            PizzaAdicional pizzaAdicionalOrphanCheck = pizza.getPizzaAdicional();
            if (pizzaAdicionalOrphanCheck != null) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Pizza (" + pizza + ") cannot be destroyed since the PizzaAdicional " + pizzaAdicionalOrphanCheck + " in its pizzaAdicional field has a non-nullable pizza field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            Sabor idSabor = pizza.getIdSabor();
            if (idSabor != null) {
                idSabor.getPizzaList().remove(pizza);
                idSabor = em.merge(idSabor);
            }
            Tipo idTipo = pizza.getIdTipo();
            if (idTipo != null) {
                idTipo.getPizzaList().remove(pizza);
                idTipo = em.merge(idTipo);
            }
            em.remove(pizza);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Pizza> findPizzaEntities() {
        return findPizzaEntities(true, -1, -1);
    }

    public List<Pizza> findPizzaEntities(int maxResults, int firstResult) {
        return findPizzaEntities(false, maxResults, firstResult);
    }

    private List<Pizza> findPizzaEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Pizza.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Pizza findPizza(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Pizza.class, id);
        } finally {
            em.close();
        }
    }

    public int getPizzaCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Pizza> rt = cq.from(Pizza.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}

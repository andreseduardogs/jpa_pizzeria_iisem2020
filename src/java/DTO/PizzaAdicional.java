/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DTO;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author madar
 */
@Entity
@Table(name = "PizzaAdicional")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PizzaAdicional.findAll", query = "SELECT p FROM PizzaAdicional p")
    , @NamedQuery(name = "PizzaAdicional.findByIdPizza", query = "SELECT p FROM PizzaAdicional p WHERE p.idPizza = :idPizza")})
public class PizzaAdicional implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "idPizza")
    private Integer idPizza;
    @JoinColumn(name = "idPizza", referencedColumnName = "idPizza", insertable = false, updatable = false)
    @OneToOne(optional = false)
    private Pizza pizza;
    @JoinColumn(name = "idIngrediente", referencedColumnName = "idIngrediente")
    @ManyToOne(optional = false)
    private IngredienteAdicional idIngrediente;

    public PizzaAdicional() {
    }

    public PizzaAdicional(Integer idPizza) {
        this.idPizza = idPizza;
    }

    public Integer getIdPizza() {
        return idPizza;
    }

    public void setIdPizza(Integer idPizza) {
        this.idPizza = idPizza;
    }

    public Pizza getPizza() {
        return pizza;
    }

    public void setPizza(Pizza pizza) {
        this.pizza = pizza;
    }

    public IngredienteAdicional getIdIngrediente() {
        return idIngrediente;
    }

    public void setIdIngrediente(IngredienteAdicional idIngrediente) {
        this.idIngrediente = idIngrediente;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPizza != null ? idPizza.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PizzaAdicional)) {
            return false;
        }
        PizzaAdicional other = (PizzaAdicional) object;
        if ((this.idPizza == null && other.idPizza != null) || (this.idPizza != null && !this.idPizza.equals(other.idPizza))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "DTO.PizzaAdicional[ idPizza=" + idPizza + " ]";
    }
    
}
